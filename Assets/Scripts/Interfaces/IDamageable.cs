using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3
{
    public interface IDamageable
    {
        public int CurrentHealth { get; }
        public int MaxHealth { get; }

        public delegate void TakeDamageEvent(int damage);
        public event TakeDamageEvent OnTakeDamage;

        public delegate void DeathEvent();
        public event DeathEvent OnDeath;

        public virtual void TakeDamage(TakeDamageEffect damageEffect) {}

    }
}
