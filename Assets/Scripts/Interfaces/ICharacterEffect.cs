using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3
{
    public interface ICharacterEffect
    {
        public virtual void ProcessEffect(CharacterManager chartacter)
        {

        }
    }
}
