using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3
{
    public class InstantCharacterEffect : ScriptableObject, ICharacterEffect
    {
        public int effectID;

        public virtual void ProcessEffect(CharacterManager character)
        {

        }
    }
}
