using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TextCore.Text;

namespace Ac1dwir3
{
    [CreateAssetMenu(menuName = "Character Effects/Instant Effects/Take Damage")]
    public class TakeDamageEffect : InstantCharacterEffect
    {
        //ALTHOUGH A CHARACETR CAN TAKE DAMAGE FROM ENVIRONMENT (TRAPS AND SUCH)
        //CHARACTERS CAN HAVE DAMAGE MODIFIERS THAT WE SHOULD CHECK FOR WHEN DEALING DAMAGE
        public CharacterManager characterCausingDamage;

        [Header("Damage")]
        public float damage;
        //OTHER TYPES OF DAMAGES GO HERE (I.E. FIRE DAMAGE, LIGHTNING DAMAGE, ETC.)

        //[Header("Build Ups")]
        //BUILD UP EFFECTS GO HERE

        [Header("Final Damage")]
        [HideInInspector]
        public int finalDamageDealt; //THIS WILL BE THE FINAL DAMAGE AFTER CALCULATING ALL MODIFIERS TO DAMAGE

        [Header("Animations")]
        public bool playDamageAnimation = true; //STATUS EFFECT DAMAGE MAY NOT WANT AN ANIMATION PLAYED EVERY TICK
        public bool manuallySelectDamageAnimation = false;
        public string damageAnimation = "Hurt";

        [Header("Sound FX")]
        public bool willPlayDamageSound = true;
        public AudioClip baseDamageSound;
        //THESE CAN BE PLAYED ON TOP OF THE BASE DAMAGE SOUND
        //EXAMPLE: A WEAPON HAS A FIRE ENCHANTMENT, PLAY BASE DAMAGE SOUND AND FIRE DAMAGE SOUND AT THE SAME TIME
        public List<AudioClip> additionalDamageSounds = new List<AudioClip>();

        [Header("Direction Damage Came From")]
        [HideInInspector]
        public float angleFromHit; //USED TO DETERMINE KNOCKBACK DIRECTION
        [HideInInspector]
        public Vector2 damageDirection; //USED TO SPAWN DAMAGE PARTICLES (BLOOD, SPARKS, ETC.)

        public override void ProcessEffect(CharacterManager character)
        {
            base.ProcessEffect(character);

            //DONT PROCESS ANYTHING ON DEAD CHARACTERS
            if (character.isDead.Value)
                return;

            //CHECK FOR I-FRAMES (INVULNERABILITY)

            CalculateDamage(character);
        }

        private void CalculateDamage(CharacterManager character)
        {
            if (!character.IsOwner)
                return;

            if (characterCausingDamage != null)
            {
                //CHECK FOR DAMAGE MODIFIERS AND APPLY THEM TO BASE DAMAGE
            }

            //CALCULATE ALL DEFENCES AND REMOVE THEM FROM THE BASE DAMAGE (ARMOR ABSORPTION, DEFENCE BUFFS)

            finalDamageDealt = Mathf.RoundToInt(AddAllDamages());

            if (finalDamageDealt <= 0)
            {
                finalDamageDealt = 1;
            }

            character.characterNetworkManager.currentHealth.Value -= finalDamageDealt;
        }

        public int CalculateDamageForNonCharacter()
        {
            if (characterCausingDamage != null)
            {
                //CHECK FOR DAMAGE MODIFIERS AND APPLY THEM TO BASE DAMAGE
            }

            //CALCULATE ALL DEFENCES AND REMOVE THEM FROM THE BASE DAMAGE (ARMOR ABSORPTION, DEFENCE BUFFS)

            finalDamageDealt = Mathf.RoundToInt(AddAllDamages());

            if (finalDamageDealt <= 0)
            {
                finalDamageDealt = 1;
            }

            return finalDamageDealt;
        }

        private float AddAllDamages()
        {
            float allDamage = damage /*+ fireDamage + lightingDamage...*/;
            return allDamage;
        }
    }
}
