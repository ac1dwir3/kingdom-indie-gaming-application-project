using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TextCore.Text;
using UnityEngine.UI;

namespace Ac1dwir3
{
    public class UI_StatBar : MonoBehaviour
    {
        private Slider _slider;
        private RectTransform _rectTransform;

        [Header("Bar Options")]
        [SerializeField]
        private bool scaleBarLengthWithStats = true;
        [SerializeField]
        private float widthScaleMultiplier = 1f;

        protected virtual void OnValidate()
        {
            _slider = GetComponent<Slider>();
            _rectTransform = GetComponent<RectTransform>();
        }

        protected virtual void Awake()
        {

        }

        protected virtual void Start()
        {

        }

        protected virtual void Update()
        {

        }

        public virtual void SetStat(int newValue)
        {
            _slider.value = newValue;
        }

        public virtual void SetMaxStat(int maxValue)
        {
            _slider.maxValue = maxValue;
            _slider.value = maxValue;

            if(scaleBarLengthWithStats)
            {
                _rectTransform.sizeDelta = new Vector2(maxValue * widthScaleMultiplier, _rectTransform.sizeDelta.y);

                PlayerUIManager.Instance.hudManager.RefreshHUD();
            }
        }
    }
}
