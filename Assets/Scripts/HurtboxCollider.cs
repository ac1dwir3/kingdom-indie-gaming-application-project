using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3
{
    [RequireComponent(typeof(Collider2D))]
    public class HurtboxCollider : MonoBehaviour
    {
        [Header("Damage")]
        public float damage;
        //OTHER TYPES OF DAMAGES GO HERE (I.E. FIRE DAMAGE, LIGHTNING DAMAGE, ETC.)

        private Collider2D col;

        private Vector2 contactPoint;

        protected List<IDamageable> objectsDamaged = new List<IDamageable>();

        private void Awake()
        {
            col = GetComponent<Collider2D>();
            col.isTrigger = true;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.transform.root.TryGetComponent<IDamageable>(out IDamageable victim))
            {
                contactPoint = collision.ClosestPoint(transform.position);

                //CHECK FOR FRIENDLY FIRE

                //CHECK IF VICTIM IS BLOCKING

                //CHECK IF VICTIM IS INVULNERABLE

                DamageVictim(victim);
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.transform.root.TryGetComponent<IDamageable>(out IDamageable victim))
            {
                if(objectsDamaged.Contains(victim))
                {
                    objectsDamaged.Remove(victim);
                }
            }
        }

        protected virtual void DamageVictim(IDamageable victim)
        {
            //DONT APPLY DAMAGE TO A VICTIM MULTIPLE TIMES IN A SINGLE ATTACK
            if (objectsDamaged.Contains(victim))
                return;

            objectsDamaged.Add(victim);

            TakeDamageEffect effect = Instantiate(WorldCharacterEffectsManager.Instance.takeDamageEffect);
            effect.damage = damage;
            //ASSIGN EFFECT'S OTHER DAMAGES TO THIS HURTBOX'S OTHER DAMAGES

            victim.TakeDamage(effect);
        }

        private void OnDisable()
        {
            objectsDamaged.Clear();
        }
    }
}
