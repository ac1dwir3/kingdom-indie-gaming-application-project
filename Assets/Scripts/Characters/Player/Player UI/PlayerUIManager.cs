using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

namespace Ac1dwir3
{
    public class PlayerUIManager : MonoBehaviour
    {
        private static PlayerUIManager _instance;
        public static PlayerUIManager Instance => _instance;

        public bool startGameAsClient;

        [HideInInspector]
        public PlayerUIHudManager hudManager;

        private void Awake()
        {
            if (_instance == null)
                _instance = this;
            else
                Destroy(gameObject);

            hudManager = GetComponentInChildren<PlayerUIHudManager>();
        }

        private void Start()
        {
            DontDestroyOnLoad(gameObject);
        }

        private void Update()
        {
            if (startGameAsClient)
            {
                StartCoroutine(CO_StartGameAsClient());
            }
        }

        private IEnumerator CO_StartGameAsClient()
        {
            //WE MUST FIRST SHUT DOWN NETWORK, BECAUSE WE STARTED AS A HOST IN THE TITLE SCREEN 
            NetworkManager.Singleton.Shutdown();
            startGameAsClient = false;
            yield return null;

            //WE THEN RESTART, AS A CLIENT
            NetworkManager.Singleton.StartClient();
        }
    }
}
