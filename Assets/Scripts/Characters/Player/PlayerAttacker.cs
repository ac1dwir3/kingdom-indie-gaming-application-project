using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ac1dwir3.Utilities;

namespace Ac1dwir3
{
    public class PlayerAttacker : CharacterAttacker
    {
        [HideInInspector]
        public PlayerManager player;

        protected override void Awake()
        {
            base.Awake();
            player = GetComponent<PlayerManager>();
        }

        public override void HandleAttack()
        {
            if (player.animator.GetBool(player.playerAnimatorManager.CANDASHATTACK_BOOL))
            {
                player.playerAnimatorManager.PlayTargetActionAnimation(player.playerAnimatorManager.DASHATTACK_ANIMSTATE, true);
            }

            //Vector3 mousePosition = UtilitiesClass.GetMouseWorldPosition();
            //attackDirection = (mousePosition - transform.position).normalized;

            base.HandleAttack();
        }
    }
}
