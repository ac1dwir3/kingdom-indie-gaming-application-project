using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

namespace Ac1dwir3
{
    public class PlayerNetworkManager : CharacterNetworkManager
    {
        [HideInInspector]
        public PlayerManager player;

        protected override void Awake()
        {
            base.Awake();

            player = GetComponent<PlayerManager>();
        }

        public void SetNewMaxHealthValue(int oldHealth, int newHealth)
        {
            maxHealth.Value = newHealth;
            PlayerUIManager.Instance.hudManager.RefreshHUD();
            currentHealth.Value = maxHealth.Value;
        }

    }
}
