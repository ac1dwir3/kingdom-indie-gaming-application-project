using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3
{
    public class PlayerAnimatorManager : CharacterAnimatorManager
    {
        [HideInInspector]
        public PlayerManager player;

        public readonly string DASH_ANIMSTATE = "Dash";
        public readonly string DASHATTACK_ANIMSTATE = "Dash Attack";
        public readonly string CANDASHATTACK_BOOL = "CanDashAttack";

        protected override void Awake()
        {
            base.Awake();
            player = GetComponent<PlayerManager>();
        }
    }
}
