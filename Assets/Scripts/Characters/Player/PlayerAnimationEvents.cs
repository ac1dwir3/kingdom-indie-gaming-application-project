using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3
{
    public class PlayerAnimationEvents : CharacterAnimationEvents
    {
        private PlayerManager player;

        protected override void Awake()
        {
            base.Awake();
            player = GetComponentInParent<PlayerManager>();
        }

        public void CanDashAttack()
        {
            if (player.IsOwner)
            {
                player.animator.SetBool(player.playerAnimatorManager.CANDASHATTACK_BOOL, true);
            }
        }
        public void CanNOTDashAttack()
        {
            if (player.IsOwner)
            {
                player.animator.SetBool(player.playerAnimatorManager.CANDASHATTACK_BOOL, false);
            }
        }
    }
}
