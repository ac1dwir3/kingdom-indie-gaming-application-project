using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3
{
    public class PlayerEffectsManager : CharacterEffectsManager
    {
        [HideInInspector]
        PlayerManager player;

        [Header("Debug Delete Later")]
        [SerializeField]
        InstantCharacterEffect instantEffectToTest;
        [SerializeField]
        bool processEffect = false;

        protected override void Awake()
        {
            base.Awake();
            player = GetComponent<PlayerManager>();
        }

        protected override void Update()
        {
            base.Update();

            DebugProcessEffect();
        }

        private void DebugProcessEffect()
        {
            if (processEffect)
            {
                processEffect = false;

                InstantCharacterEffect effect = Instantiate(instantEffectToTest);
                effect.ProcessEffect(player);
            }
        }
    }
}
