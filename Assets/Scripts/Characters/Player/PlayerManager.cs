using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3
{
    [RequireComponent(typeof(PlayerLocomotionManager))]
    [RequireComponent(typeof(PlayerAnimatorManager))]
    public class PlayerManager : CharacterManager
    {
        public bool respawnCharacter = false;

        [HideInInspector]
        public PlayerLocomotionManager playerLocomotionManager;
        [HideInInspector]
        public PlayerAnimatorManager playerAnimatorManager;
        [HideInInspector]
        public PlayerNetworkManager playerNetworkManager;
        [HideInInspector]
        public PlayerAnimationEvents playerAnimationEvents;
        [HideInInspector]
        public PlayerAttacker playerAttacker;
        [HideInInspector]
        public PlayerStatsManager playerStatsManager;

        [Space]
        [SerializeField]
        private GameObject cameraControllerPrefab;

        protected override void Awake()
        {
            base.Awake();

            playerLocomotionManager = GetComponent<PlayerLocomotionManager>();
            playerAnimatorManager = GetComponent<PlayerAnimatorManager>();
            playerNetworkManager = GetComponent<PlayerNetworkManager>();
            playerAnimationEvents = GetComponentInChildren<PlayerAnimationEvents>();
            playerAttacker = GetComponent<PlayerAttacker>();
            playerStatsManager = GetComponent<PlayerStatsManager>();

        }

        protected override void Update()
        {
            base.Update();

            // IF WE DO NOT OWN THIS OBJECT, WE DON'T CONTROL OR EDIT IT
            if (!IsOwner)
                return;

            CheckRevivePlayer();
            HandleLocomotionStateMachine();
        }

        private void HandleLocomotionStateMachine()
        {
            switch (playerLocomotionManager.state)
            {
                case PlayerLocomotionManager.State.Normal:
                    playerLocomotionManager.HandleMovement();
                    break;
                case PlayerLocomotionManager.State.Dash:
                    playerLocomotionManager.HandleDash();
                    break;
            }
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();

            // IF WE DO NOT OWN THIS OBJECT, WE DON'T CONTROL OR EDIT IT
            if (!IsOwner)
                return;

        }

        public override void OnNetworkSpawn()
        {
            base.OnNetworkSpawn();

            if (IsOwner)
            {
                CameraController.Instance.target = transform;
                PlayerInputManager.Instance.player = this;

                PlayerUIManager.Instance.hudManager.gameObject.SetActive(true);
                playerNetworkManager.currentHealth.OnValueChanged += PlayerUIManager.Instance.hudManager.SetNewHealthValue;

                playerNetworkManager.maxHealth.Value = 10;
                PlayerUIManager.Instance.hudManager.SetMaxHealthValue(playerNetworkManager.maxHealth.Value);
            }

            playerNetworkManager.currentHealth.OnValueChanged += playerNetworkManager.CheckHP;
        }

        public override IEnumerator ProcessDeathEvent_CO()
        {
            //CHECK IF ALL PLAYERS ARE DEAD, IF SO RESPAWN ALL OF THEM

            return base.ProcessDeathEvent_CO();
        }

        public override void ReviveCharacter()
        {
            base.ReviveCharacter();

            if(IsOwner)
            {
                playerNetworkManager.currentHealth.Value = playerNetworkManager.maxHealth.Value;

                //PLAY REVIVE FX

                playerAnimatorManager.PlayTargetActionAnimation("Empty", false);
            }
        }

        public void CheckRevivePlayer()
        {
            if(respawnCharacter)
            {
                respawnCharacter = false;
                ReviveCharacter();
            }
        }
    }
}
