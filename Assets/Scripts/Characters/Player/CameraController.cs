using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

namespace Ac1dwir3
{
    public class CameraController : MonoBehaviour
    {
        private static CameraController _instance;
        public static CameraController Instance => _instance;

        public Transform target;

        [Header ("Camera Settings")]
        [SerializeField]
        private bool smoothFollow;
        [SerializeField]
        private float smoothTime = 0.1f;

        [SerializeField]
        private Vector3 positionOffset = Vector3.zero;
        private Vector3 _velocity;

        private void Awake()
        {
            if (_instance == null)
                _instance = this;
            else
                Destroy(gameObject);
        }

        private void Start()
        {
            DontDestroyOnLoad(gameObject);

            positionOffset = transform.position;

            if (target != null)
                transform.position = target.position + positionOffset;
        }

        private void Update()
        {
            if (target != null)
            {
                FollowTarget(smoothFollow);
            }
        }

        private void FollowTarget(bool damped)
        {
            if (!damped)
            {
                transform.position = target.position + positionOffset;
            }
            else
            {
                transform.position = Vector3.SmoothDamp(transform.position, target.position + positionOffset, ref _velocity, smoothTime * Time.deltaTime);
            }
        }
    }
}
