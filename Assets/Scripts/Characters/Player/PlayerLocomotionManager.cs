using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerLocomotionManager : CharacterLocomotionManager
    {
        [HideInInspector]
        public PlayerManager player;

        public enum State
        {
            Normal,
            Dash,
        }
        public State state;

        #region Inputs
        private float verticalMovement = 0f;
        private float horizontalMovement = 0f;
        #endregion

        [Header("Dashing")]
        [SerializeField]
        private float dashSpeed = 40f;
        [SerializeField]
        private float dashSpeedDropMultiplier = 4f;
        [SerializeField]
        private float dashSpeedMinimum;

        private float currentDashSpeed;
        private Vector2 dashDirection;
        private Vector2 lastMovementDirection;

        protected override void Awake()
        {
            base.Awake();
            player = GetComponent<PlayerManager>();

            state = State.Normal;
        }

        protected override void Update()
        {
            base.Update();

            GetAllInputs();

            if (player.IsOwner)
            {
                player.playerNetworkManager.networkMoveAmount.Value = moveAmount;
            }
            else
            {
                moveAmount = player.playerNetworkManager.networkMoveAmount.Value;

                player.playerAnimatorManager.UpdateAnimatorMovementParameters(moveAmount);
            }

            switch (state)
            {
                case State.Normal:
                    CalculateMovementDirectionFromInput();
                    OrientSprite();
                    break;
                case State.Dash:
                    ReduceDashSpeedOverTime();
                    break;
            }
        }

        private void GetAllInputs()
        {
            GetMovementInputs();
        }

        private void GetMovementInputs()
        {
            horizontalMovement = PlayerInputManager.Instance.MovementInput.x;
            verticalMovement = PlayerInputManager.Instance.MovementInput.y;
            moveAmount = PlayerInputManager.Instance.moveAmount;

        }

        private void CalculateMovementDirectionFromInput()
        {
            moveDirection = new Vector2(horizontalMovement, verticalMovement);
            moveDirection.Normalize();
            moveDirection.z = 0;

            if (horizontalMovement != 0 || verticalMovement != 0)
                lastMovementDirection = moveDirection;
        }

        public override void HandleMovement()
        {
            base.HandleMovement();
            rb.velocity *= new Vector2(moveAmount, moveAmount);
        }

        private void ReduceDashSpeedOverTime()
        {
            currentDashSpeed -= currentDashSpeed * dashSpeedDropMultiplier * Time.deltaTime;

            if (currentDashSpeed <= dashSpeedMinimum)
            {
                state = State.Normal;
            }
        }

        public void AttemptToDash()
        {
            if (player.isPerformingAction)
                return;

            dashDirection = lastMovementDirection;
            currentDashSpeed = dashSpeed;
            player.playerAnimatorManager.PlayTargetActionAnimation(player.playerAnimatorManager.DASH_ANIMSTATE, true);
            state = State.Dash;
        }

        public void HandleDash()
        {
            rb.velocity = dashDirection * currentDashSpeed;
        }
    }
}