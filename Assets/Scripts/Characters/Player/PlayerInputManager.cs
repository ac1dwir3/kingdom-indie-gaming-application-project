using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Ac1dwir3
{
    public class PlayerInputManager : MonoBehaviour
    {
        private static PlayerInputManager _instance;
        public static PlayerInputManager Instance => _instance;

        PlayerControls playerControls;

        [HideInInspector]
        public PlayerManager player;

        #region Movement
        [Header("Movement")]
        [SerializeField]
        private Vector2 movementInput;
        public Vector2 MovementInput => movementInput;
        [HideInInspector]
        public float verticalMovement;
        [HideInInspector]
        public float horizontalMovement;
        public float moveAmount;

        [SerializeField]
        private bool clampMovement = false; //FOR GAMEPAD SUPPORT IF EVER IMPLEMENTED
        #endregion

        #region Combat
        [Header("Combat")]
        [SerializeField]
        private bool dashInput;
        public bool DashInput => dashInput;
        [SerializeField]
        private bool attackInput;
        public bool AttackInput => attackInput;
        #endregion

        private void Awake()
        {
            if (_instance == null)
                _instance = this;
            else
                Destroy(gameObject);
        }

        private void Start()
        {
            DontDestroyOnLoad(gameObject);

            SceneManager.activeSceneChanged += OnSceneChange;

            _instance.enabled = false;
        }

        private void OnSceneChange(Scene oldScene, Scene newScene)
        {
            //IF LOADING INTO THE WORLD SCENE, ENABLE PLAYER CONTROLS
            if (newScene.buildIndex == WorldSaveGameManager.Instance.WorldSceneIndex)
            {
                _instance.enabled = true;
            }
            //IF LOADING INTO ANY OTHER SCENE, DISABLE PLAYER CONTROLS
            //THIS PREVENTS MOVING PLAYER AROUND WHEN IN MAIN MENU, CHARACTER CREATION SCREEN, ETC.)
            else
            {
                _instance.enabled = false;
            }
        }

        private void OnEnable()
        {
            if (playerControls == null)
            {
                playerControls = new PlayerControls();

                playerControls.Player.Movement.performed += i => movementInput = i.ReadValue<Vector2>();
                playerControls.Player.Dash.performed += i => dashInput = true;
                playerControls.Player.Attack.performed += i => attackInput = true;
            }

            playerControls.Enable();
        }

        private void OnDestroy()
        {
            SceneManager.activeSceneChanged -= OnSceneChange;
        }

        private void Update()
        {
            if (!player.IsOwner)
                return;

            HandleAllInputs();
        }

        private void HandleAllInputs()
        {
            HandleMovementInput();
            HandleDashInput();
            HandleAttackInput();
        }

        private void HandleMovementInput()
        {
            verticalMovement = movementInput.y;
            horizontalMovement = movementInput.x;
            moveAmount = Mathf.Clamp01(Mathf.Abs(verticalMovement) + Mathf.Abs(horizontalMovement));

            if (clampMovement)
            {
                if (moveAmount <= 0.5f && moveAmount > 0f)
                {
                    moveAmount = 0.5f;
                }
                else if (moveAmount > 0.5f && moveAmount <= 1f)
                {
                    moveAmount = 1f;
                }
            }

            if (player == null)
                return;

            player.playerAnimatorManager.UpdateAnimatorMovementParameters(moveAmount);
        }

        private void HandleDashInput()
        {
            if (dashInput == true)
            {
                dashInput = false;

                player.playerLocomotionManager.AttemptToDash();
            }
        }

        private void HandleAttackInput()
        {
            if(attackInput == true)
            {
                attackInput = false;

                player.playerAttacker.HandleAttack();
            }
        }

        private void OnApplicationFocus(bool focus)
        {
            if (enabled)
            {
                if (focus)
                {
                    playerControls.Enable();
                }
                else
                {
                    playerControls.Disable();
                }
            }
        }
    }
}
