using UnityEngine;

namespace Ac1dwir3
{
    public class EnemyNetworkManager : CharacterNetworkManager
    {
        [HideInInspector]
        public EnemyManager enemy;

        protected override void Awake()
        {
            base.Awake();

            enemy = GetComponent<EnemyManager>();
        }
    }
}
