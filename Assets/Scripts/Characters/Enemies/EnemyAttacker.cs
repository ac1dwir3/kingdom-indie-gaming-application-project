using UnityEngine;

namespace Ac1dwir3
{
    public class EnemyAttackManager : CharacterAttacker
    {
        [HideInInspector]
        public EnemyManager enemy;

        public float nextAttackTime;
        public float attackTime;

        protected override void Awake()
        {
            base.Awake();
            
            enemy = GetComponent<EnemyManager>();
            GetAttackTime(enemy.enemyAnimatorManager.attackAnimClip);
        }

        public void GetAttackTime(AnimationClip attackAnimation)
        {
            attackTime = attackAnimation.length;
        }

        public override void HandleAttack()
        {
            base.HandleAttack();
        }
    }
}
