using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3
{
    public class EnemyEffectsManager : CharacterEffectsManager
    {
        [HideInInspector]
        public EnemyManager enemy;

        protected override void Awake()
        {
            base.Awake();

            enemy = GetComponent<EnemyManager>();
        }

        protected override void Update()
        {
        
        }
    }
}
