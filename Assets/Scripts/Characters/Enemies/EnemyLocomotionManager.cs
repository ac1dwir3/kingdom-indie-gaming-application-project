using Ac1dwir3.Utilities;
using System.Collections;
using UnityEngine;

namespace Ac1dwir3
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class EnemyLocomotionManager : CharacterLocomotionManager
    {
        [HideInInspector]
        public EnemyManager enemy;

        [SerializeField]
        private Vector3 desiredPosition;

        public Vector3 startingPosition;

        [SerializeField]
        Vector3 roamPosition;

        [SerializeField]
        float reachedPositionDistance = 1f;

        [SerializeField]
        private float minRoamDistance = 10f;
        [SerializeField]
        private float maxRoamDistance = 70f;

        public Transform targetToChase = null;
        float targetRange = 5f;
        float attackRange = 1.5f;
        float stopChaseTargetDistance = 12f;


        protected override void Awake()
        {
            base.Awake();
            enemy = GetComponent<EnemyManager>();
            enemy.state = EnemyManager.State.Roaming;
        }

        protected override void Start()
        {
            base.Start();

            startingPosition = transform.position;
            roamPosition = GetRoamingPosition();
        }

        protected override void Update()
        {
            base.Update();

            OrientSprite();

            moveAmount = Mathf.Clamp01(rb.velocity.magnitude);

            if (enemy.IsOwner)
            {
                enemy.enemyNetworkManager.networkMoveAmount.Value = moveAmount;
            }
            else
            {
                moveAmount = enemy.enemyNetworkManager.networkMoveAmount.Value;

                enemy.enemyAnimatorManager.UpdateAnimatorMovementParameters(moveAmount);
            }

            moveDirection = (desiredPosition - transform.position).normalized;
        }

        protected override void FixedUpdate()
        {
            HandleMovement();
        }

        public void SetDesiredPosition(Vector3 desiredPosition)
        {
            this.desiredPosition = desiredPosition;
        }

        public Vector3 GetRoamingPosition()
        {
            return startingPosition + UtilitiesClass.GetRandomDirection() * Random.Range(minRoamDistance, maxRoamDistance);
        }

        public void Roam()
        {
            SetDesiredPosition(roamPosition);

            if (Vector3.Distance(transform.position, desiredPosition) < reachedPositionDistance)
            {
                //REACHED ROAM POSITION
                roamPosition = GetRoamingPosition();
            }
        }

        public void ChaseTarget()
        {

            PlayerManager player = targetToChase.GetComponent<PlayerManager>();
            if (player != null)
            {
                if (player.isDead.Value)
                {
                    enemy.state = EnemyManager.State.ReturningToStart;
                    return;
                }
            }

            SetDesiredPosition(targetToChase.position);
            if (Vector3.Distance(transform.position, desiredPosition) < attackRange)
            {
                //TARGET IS WITHING ATTACK RANGE
                if (Time.time > enemy.enemyAttacker.nextAttackTime) //ATTACK HAS COOLED DOWN
                {
                    SetDesiredPosition(transform.position);

                    enemy.enemyAttacker.nextAttackTime = Time.time + enemy.enemyAttacker.attackTime;
                    enemy.enemyAttacker.HandleAttack();
                }
            }

            if (Vector3.Distance(transform.position, desiredPosition) > stopChaseTargetDistance)
            {
                //TARGET IS TOO FAR, STOP CHASING
                enemy.state = EnemyManager.State.ReturningToStart;
            }
        }

        public void FindTarget()
        {
            Collider2D[] players = Physics2D.OverlapCircleAll(transform.position, targetRange, enemy.playerLayer);

            if (players != null && players.Length > 0 && players[0] != null)
            {
                //A PLAYER IS WITHIN ALERT RANGE
                PlayerManager closestPlayer = null;
                for (int i = 0; i < players.Length; i++)
                {
                    PlayerManager player = players[i].transform.root.GetComponent<PlayerManager>();
                    if (player.isDead.Value)
                        continue;

                    if (closestPlayer == null)
                    {
                        closestPlayer = player;
                    }
                    else
                    {
                        if (Vector3.Distance(transform.position, player.transform.position) < Vector3.Distance(transform.position, closestPlayer.transform.position))
                        {
                            closestPlayer = player;
                        }
                    }
                }

                if (closestPlayer == null) //IF ALL NEARBY PLAYERS ARE DEAD
                    return;

                targetToChase = closestPlayer.transform;
                enemy.state = EnemyManager.State.ChasingTarget;
            }
        }

        public void ReturnToStart()
        {
            SetDesiredPosition(startingPosition);

            if (Vector3.Distance(transform.position, desiredPosition) < reachedPositionDistance)
            {
                enemy.state = EnemyManager.State.Roaming;
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, targetRange);

            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(transform.position, stopChaseTargetDistance);

            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, attackRange);
        }
    }
}
