using UnityEngine;

namespace Ac1dwir3
{
    public class EnemyAnimatorManager : CharacterAnimatorManager
    {
        [HideInInspector]
        public EnemyManager enemy;

        public AnimationClip attackAnimClip;

        protected override void Awake()
        {
            base.Awake();

            enemy = GetComponent<EnemyManager>();
        }
    }
}
