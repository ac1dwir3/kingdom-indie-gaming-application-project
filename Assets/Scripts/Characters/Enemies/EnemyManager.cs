using System.Collections;
using UnityEngine;

namespace Ac1dwir3
{
    [RequireComponent(typeof(EnemyLocomotionManager))]
    [RequireComponent(typeof(EnemyAnimatorManager))]
    public class EnemyManager : CharacterManager
    {
        [HideInInspector]
        public EnemyAnimatorManager enemyAnimatorManager;
        [HideInInspector]
        public EnemyAttackManager enemyAttacker;
        [HideInInspector]
        public EnemyEffectsManager enemyEffectsManager;
        [HideInInspector]
        public EnemyLocomotionManager enemyLocomotionManager;
        [HideInInspector]
        public EnemyNetworkManager enemyNetworkManager;
        [HideInInspector]
        public EnemyStatsManager enemyStatsManager;

        public LayerMask playerLayer;

        public enum State
        {
            Roaming,
            ChasingTarget,
            Attacking,
            ReturningToStart,
        }
        public State state = State.Roaming;

        protected override void Awake()
        {
            base.Awake();

            enemyAnimatorManager = GetComponent<EnemyAnimatorManager>();
            enemyAttacker = GetComponent<EnemyAttackManager>();
            enemyEffectsManager = GetComponent<EnemyEffectsManager>();
            enemyLocomotionManager = GetComponent<EnemyLocomotionManager>();
            enemyNetworkManager = GetComponent<EnemyNetworkManager>();
            enemyStatsManager = GetComponent<EnemyStatsManager>();
        }

        protected override void Start()
        {
            base.Start();

            networkObject.Spawn();
        }

        protected override void Update()
        {
            base.Update();

            // IF WE DO NOT OWN THIS OBJECT, WE DON'T CONTROL OR EDIT IT
            if (!IsOwner)
                return;

            HandleStateMachine();
        }

        public override IEnumerator ProcessDeathEvent_CO()
        {
            base.ProcessDeathEvent_CO();

            yield return null;

        }

        public override void OnNetworkSpawn()
        {
            base.OnNetworkSpawn();

            if (IsOwner)
            {
                enemyNetworkManager.maxHealth.Value = 20;
            }

            enemyNetworkManager.currentHealth.OnValueChanged += enemyNetworkManager.CheckHP;
        }

        protected override void HandleStateMachine()
        {
            base.HandleStateMachine();

            switch (state)
            {
                case State.Roaming:
                    enemyLocomotionManager.Roam();
                    enemyLocomotionManager.FindTarget();
                    break;
                case State.ChasingTarget:
                    enemyLocomotionManager.ChaseTarget();
                    break;
                case State.Attacking:
                    break;
                case State.ReturningToStart:
                    enemyLocomotionManager.ReturnToStart();
                    break;
            }
        }
    }
}
