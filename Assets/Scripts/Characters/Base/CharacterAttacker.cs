using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3
{
    public class CharacterAttacker : MonoBehaviour
    {
        [HideInInspector]
        public CharacterManager character;

        //public Vector2 attackDirection = Vector2.zero;

        protected virtual void Awake()
        {
            character = GetComponent<CharacterManager>();
        }

        public virtual void HandleAttack()
        {
            if (character.isPerformingAction)
                return;

            character.characterAnimatorManager.PlayTargetActionAnimation(character.characterAnimatorManager.ATTACK_ANIMSTATE, true);
        }
    }
}
