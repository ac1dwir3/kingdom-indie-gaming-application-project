using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3
{
    public class CharacterAnimationEvents : MonoBehaviour
    {
        private CharacterManager character;

        protected virtual void Awake()
        {
            character = GetComponentInParent<CharacterManager>();
        }

        public void EnableHurtbox()
        {

        }
        public void DisableHurtbox()
        {

        }
    }
}
