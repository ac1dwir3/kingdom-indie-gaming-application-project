using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.TextCore.Text;

namespace Ac1dwir3
{
    public class CharacterManager : NetworkBehaviour, IDamageable
    {
        [Header("Status")]
        public NetworkVariable<bool> isDead = new NetworkVariable<bool>(false, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);

        [HideInInspector]
        public CharacterNetworkManager characterNetworkManager;
        [HideInInspector]
        public CharacterLocomotionManager characterLocomotionManager;
        [HideInInspector]
        public CharacterAnimatorManager characterAnimatorManager;
        [HideInInspector]
        public CharacterStatsManager characterStatsManager;
        [HideInInspector]
        public CharacterEffectsManager characterEffectsManager;
        [HideInInspector]
        public CharacterAnimationEvents characterAnimationEvents;
        [HideInInspector]
        public CharacterAttacker characterAttacker;

        [HideInInspector]
        public Animator animator;

        [HideInInspector]
        public NetworkObject networkObject;

        [Header("Flags")]
        public bool isPerformingAction = false;
        public bool canRotate = true;
        public bool canMove = true;

        public event IDamageable.TakeDamageEvent OnTakeDamage;
        public event IDamageable.DeathEvent OnDeath;

        public int CurrentHealth => characterNetworkManager.currentHealth.Value;
        public int MaxHealth => characterNetworkManager.maxHealth.Value;

        protected virtual void Awake()
        {
            DontDestroyOnLoad(this);
            characterNetworkManager = GetComponent<CharacterNetworkManager>();
            characterLocomotionManager = GetComponent<CharacterLocomotionManager>();
            characterAnimatorManager = GetComponent<CharacterAnimatorManager>();
            characterStatsManager = GetComponent<CharacterStatsManager>();
            characterEffectsManager = GetComponent<CharacterEffectsManager>();
            characterAnimationEvents = GetComponentInChildren<CharacterAnimationEvents>();
            characterAttacker = GetComponent<CharacterAttacker>();

            animator = GetComponentInChildren<Animator>();
            networkObject = GetComponent<NetworkObject>();
        }

        protected virtual void Start()
        {
            characterNetworkManager.currentHealth.Value = MaxHealth;
        }

        public virtual IEnumerator ProcessDeathEvent_CO()
        {
            if (IsOwner)
            {
                isDead.Value = true;

                //RESET ANY FLAGS THAT NEED TO BE RESET HERE

                characterAnimatorManager.PlayTargetActionAnimation(characterAnimatorManager.DEATH_ANIMSTATE, true);


            }

            //PLAY DEATH SOUND FX

            //DROP ITEMS

            yield return new WaitForSeconds(5);

            //DESPAWN CHARACTER
        }

        protected virtual void Update()
        {
            if (IsOwner)
            {
                characterNetworkManager.networkPosition.Value = transform.position;
                characterNetworkManager.networkRotation.Value = transform.localScale;
            }
            else
            {
                transform.position = Vector3.SmoothDamp(transform.position, characterNetworkManager.networkPosition.Value, ref characterNetworkManager.NetworkPositionVelocity, characterNetworkManager.NetworkPositionSmoothTime);

                transform.localScale = characterNetworkManager.networkRotation.Value;
            }
        }

        protected virtual void FixedUpdate()
        {

        }

        public void TakeDamage(TakeDamageEffect damageEffect)
        {
            characterEffectsManager.ProcessInstantEffects(damageEffect);
            OnTakeDamage?.Invoke(damageEffect.finalDamageDealt);

            if (CurrentHealth <= 0)
            {
                OnDeath?.Invoke();
            }
            else
            {
                if (damageEffect.playDamageAnimation)
                {
                    if (!damageEffect.manuallySelectDamageAnimation)
                    {
                        characterAnimatorManager.PlayTargetActionAnimation(damageEffect.damageAnimation, true);
                    }
                    else
                    {
                        //characterAnimatorManager.PlayTargetActionAnimation(____DESIRED DAMAGE ANIMATION HERE____, true);
                    }
                }
            }
        }

        public virtual void ReviveCharacter()
        {
            isDead.Value = false;
        }

        protected virtual void HandleStateMachine()
        {

        }
    }
}
