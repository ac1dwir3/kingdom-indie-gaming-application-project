using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace Ac1dwir3
{
    public class CharacterAnimatorManager : MonoBehaviour
    {
        [HideInInspector]
        public CharacterManager character;

        public readonly string MOVEAMOUNT_FLOAT = "Movement_Magnitude";

        public readonly string DEATH_ANIMSTATE = "Death";
        public readonly string ATTACK_ANIMSTATE = "Attack";

        protected virtual void Awake()
        {
            character = GetComponent<CharacterManager>();
        }

        protected virtual void Start()
        {

        }

        protected virtual void Update()
        {

        }

        public virtual void UpdateAnimatorMovementParameters(float moveAmount)
        {
            character.animator.SetFloat(MOVEAMOUNT_FLOAT, moveAmount);
        }

        public void PlayTargetActionAnimation(string targetAnimationState, bool isPerformingAction, bool canRotate = false, bool canMove = false)
        {
            character.animator.Play(targetAnimationState);

            //CAN BE USED TO PREVENT CHARACTER FROM ATTEMPTING OTHER ACTIONS WHILE ALREADY DOING ONE
            //EXAMPLE: IF A CHARACTER GETS HURT AND PLAYS A HURT ANIMATION, THIS WILL PREVENT THEM FROM INTERRUPTING THE HURT ANIMATION TO ATTACK
            character.isPerformingAction = isPerformingAction;
            character.canRotate = canRotate;
            character.canMove = canMove;

            //TELL SERVER/HOST WE PLAYED AN ANIMATION, AND TO PLAY THAT ANIMATION FOR EVERYONE ELSE
            if (character.IsOwner)
                character.characterNetworkManager.NotifyServerOfActionAnimationServerRpc(NetworkManager.Singleton.LocalClientId, targetAnimationState);
        }
    }
}
