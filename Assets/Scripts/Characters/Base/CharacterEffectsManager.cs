using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3
{
    public class CharacterEffectsManager : MonoBehaviour
    {
        [HideInInspector]
        public CharacterManager characterManager;

        // 3 TYPES OF CHARACTER EFFECTS:
        //  INSTANT EFFECTS (TAKING DAMAGE, HEALING, ETC.)
        //  TIMED EFFECTS (BUILD UPS I.E. POISON DAMAGE, FIRE DAMAGE, AND SUCH) - NOT IMPLEMENTED
        //  STATIC EFFECTS (BUFFS AND DEBUFFS I.E. ARMOR, ENCHANTMENTS, AND SUCH) - NOT IMPLEMENTED

        protected virtual void Awake()
        {
            characterManager = GetComponent<CharacterManager>();
        }

        protected virtual void Update()
        {

        }

        public virtual void ProcessInstantEffects(InstantCharacterEffect effect)
        {
            effect.ProcessEffect(characterManager);
        }
    }
}
