using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

namespace Ac1dwir3
{
    public class CharacterNetworkManager : NetworkBehaviour
    {
        [HideInInspector]
        public CharacterManager character;

        [Header("Transform")]
        public NetworkVariable<Vector3> networkPosition = new NetworkVariable<Vector3>(Vector3.zero, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);
        public NetworkVariable<Vector3> networkRotation = new NetworkVariable<Vector3>(Vector3.one, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);
        [HideInInspector]
        public Vector3 NetworkPositionVelocity;
        public float NetworkPositionSmoothTime = 0.1f;

        [Header("Animator")]
        public NetworkVariable<float> networkMoveAmount = new NetworkVariable<float>(0f, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);

        [Header("Stats")]
        public NetworkVariable<int> currentHealth = new NetworkVariable<int>(0, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);
        public NetworkVariable<int> maxHealth = new NetworkVariable<int>(0, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);

        protected virtual void Awake()
        {
            character = GetComponent<CharacterManager>();
        }

        public void CheckHP(int oldValue, int newValue)
        {
            if(currentHealth.Value <= 0)
            {
                StartCoroutine(character.ProcessDeathEvent_CO());
            }

            //PREVENTS OVER HEALING
            if(character.IsOwner)
            {
                if(currentHealth.Value > maxHealth.Value)
                {
                    currentHealth.Value = maxHealth.Value;
                }
            }
        }

        [ServerRpc]
        public void NotifyServerOfActionAnimationServerRpc(ulong clientID, string animationID)
        {
            //ONLY SERVER/HOST CAN CALL THIS
            if (IsServer)
            {
                PlayActionAnimationForAllClientsClientRpc(clientID, animationID);
            }
        }

        [ClientRpc]
        public void PlayActionAnimationForAllClientsClientRpc(ulong clientID, string animationID)
        {
            //MAKE SURE NOT TO RUN THE FUNCTION ON THE CLIENT WHO MADE THIS REQUEST
            if (clientID != NetworkManager.Singleton.LocalClientId)
            {
                PerformActionAnimationFromServer(animationID);
            }
        }

        private void PerformActionAnimationFromServer(string animationID)
        {
            character.animator.Play(animationID);
        }
    }
}
