using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class CharacterLocomotionManager : MonoBehaviour
    {
        [HideInInspector]
        public CharacterManager character;

        [HideInInspector]
        public Rigidbody2D rb;

        protected Vector3 moveDirection;

        [Header("Movement Variables")]
        [SerializeField]
        protected float movementSpeed = 10f;
        protected float moveAmount;

        protected virtual void Awake()
        {
            character = GetComponent<CharacterManager>();
            rb = GetComponent<Rigidbody2D>();
        }

        protected virtual void Start()
        {

        }

        protected virtual void Update()
        {

        }

        protected virtual void FixedUpdate()
        {

        }

        public virtual void HandleMovement()
        {
            if (!character.canMove)
            {
                rb.velocity = Vector2.zero;
                return;
            }

            rb.velocity = moveDirection * movementSpeed;

            character.characterAnimatorManager.UpdateAnimatorMovementParameters(moveAmount);
        }

        protected void OrientSprite()
        {
            if (!character.canRotate)
                return;

            //DEFAULT SPRITE SHOULD ALWAYS FACE RIGHT, SO MOVING LEFT (< 0) WILL FLIP SPRITE AND COLLIDER

            if (rb.velocity.x < 0) //MOVING LEFT
            {
                character.transform.localScale = new Vector3(-1, 1, 1);
            }
            else if (rb.velocity.x > 0) //MOVING RIGHT
            {
                character.transform.localScale = new Vector3(1, 1, 1);
            }
        }
    }
}
