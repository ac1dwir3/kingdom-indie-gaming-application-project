using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3
{
    public class DamageableObject : MonoBehaviour, IDamageable
    {
        protected int _currentHealth = 10;
        public int CurrentHealth => _currentHealth;

        protected int _maxHealth = 10;
        public int MaxHealth => _maxHealth;

        public event IDamageable.TakeDamageEvent OnTakeDamage;
        public event IDamageable.DeathEvent OnDeath;

        public virtual void TakeDamage(TakeDamageEffect damageEffect)
        {
            _currentHealth -= damageEffect.CalculateDamageForNonCharacter();
            OnTakeDamage?.Invoke(damageEffect.finalDamageDealt);

            Debug.Log($"Damage Dealt: {damageEffect.finalDamageDealt}.\nCurrent Health: {CurrentHealth}.");

            if(_currentHealth <= 0)
            {
                OnDeath?.Invoke();
                Destroy(gameObject);
            }
        }
    }
}
