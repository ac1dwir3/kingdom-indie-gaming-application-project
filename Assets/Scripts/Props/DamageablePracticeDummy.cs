using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3
{
    public class DamageablePracticeDummy : DamageableObject
    {
        public override void TakeDamage(TakeDamageEffect damageEffect)
        {
            if(_currentHealth < _maxHealth)
            {
                _currentHealth = _maxHealth;
            }
            base.TakeDamage(damageEffect);
        }
    }
}
