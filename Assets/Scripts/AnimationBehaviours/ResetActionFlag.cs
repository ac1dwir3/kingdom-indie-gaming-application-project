using Ac1dwir3;
using UnityEngine;

public class ResetActionFlag : StateMachineBehaviour
{
    CharacterManager character;
    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (character == null)
            character = animator.GetComponentInParent<CharacterManager>();

        character.isPerformingAction = false;
        character.canMove = true;
        character.canRotate = true;
    }
}
