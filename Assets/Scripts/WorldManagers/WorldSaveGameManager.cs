using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Ac1dwir3
{
    public class WorldSaveGameManager : MonoBehaviour
    {
        private static WorldSaveGameManager _instance;
        public static WorldSaveGameManager Instance => _instance;

        [SerializeField]
        private int worldSceneIndex = 1;
        public int WorldSceneIndex => worldSceneIndex;

        private void Awake()
        {
            if(_instance == null)
                _instance = this;
            else
                Destroy(gameObject);
        }

        private void Start()
        {
            DontDestroyOnLoad(gameObject);
        }

        public IEnumerator CO_LoadWorldScene()
        {
            AsyncOperation loadOperation = SceneManager.LoadSceneAsync(worldSceneIndex);

            yield return null;
        }
    }
}
