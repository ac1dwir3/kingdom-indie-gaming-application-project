using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace Ac1dwir3
{
    public class WorldAudioManager : MonoBehaviour
    {
        private static WorldAudioManager _instance;
        public static WorldAudioManager Instance => _instance;

        public AudioMixer audioMixer;

        private void Awake()
        {
            if (_instance == null)
                _instance = this;
            else
                Destroy(gameObject);
        }

        private void Start()
        {
            DontDestroyOnLoad(gameObject);
        }


    }
}
