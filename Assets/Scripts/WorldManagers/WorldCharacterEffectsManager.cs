using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3
{
    public class WorldCharacterEffectsManager : MonoBehaviour
    {
        private static WorldCharacterEffectsManager _instance;
        public static WorldCharacterEffectsManager Instance => _instance;

        [Header("Frequently Used Effects")]
        [SerializeField]
        public TakeDamageEffect takeDamageEffect;

        [Header("All Effects")]
        [SerializeField]
        List<InstantCharacterEffect> instantEffects;


        private void Awake()
        {
            if (_instance == null)
                _instance = this;
            else
                Destroy(gameObject);

            GenerateEffectIDs();
        }

        private void Start()
        {
            DontDestroyOnLoad(gameObject);
        }

        private void GenerateEffectIDs()
        {
            for(int i = 0; i < instantEffects.Count; i++)
            {
                instantEffects[i].effectID = i;
            }
        }
    }
}
