using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Ac1dwir3
{
    [ExecuteInEditMode]
    public class SliderTextDisplay : MonoBehaviour
    {
        [SerializeField]
        private Slider slider;
        [SerializeField]
        private TextMeshProUGUI textDisplay;

        private void Awake()
        {
            if(slider != null)
            {
                slider.onValueChanged.AddListener(delegate { UpdateDisplayText(); });
            }
        }

        public void UpdateDisplayText()
        {
            textDisplay.text = slider.value.ToString();
        }

        private void OnDestroy()
        {
            slider.onValueChanged.RemoveListener(delegate { UpdateDisplayText(); });
        }
    }
}
