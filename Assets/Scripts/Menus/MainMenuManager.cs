using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

namespace Ac1dwir3
{
    public class MainMenuManager : MonoBehaviour
    {
        public void StartNetworkAsHost()
        {
            NetworkManager.Singleton.StartHost();
        }

        public void StartNewGame()
        {
            StartCoroutine(WorldSaveGameManager.Instance.CO_LoadWorldScene());
        }

        public void QuitGame()
        {
            NetworkManager.Singleton.Shutdown();
            Application.Quit();
        }

        public void SetNetworkAsClient(bool startAsClient)
        {
            PlayerUIManager.Instance.startGameAsClient = startAsClient;
        }
    }
}
