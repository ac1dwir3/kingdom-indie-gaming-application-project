using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Text;

namespace Ac1dwir3
{
    public class ResolutionSetter : MonoBehaviour
    {
        [SerializeField]
        private TMP_Dropdown dropdown;

        private readonly List<string> resolutions = new List<string>() { "640x360", "640x480", "800x600", "1024x768", "1280x720", "1280x1024", "1360x768", "1366x768", "1440x900", "1536x864", "1600x900", "1600x1200", "1680x1050", "1920x1080", "1920x1200", "2048x1152", "2048x1536", "2560x1080", "2560x1440", "2560x1600", "3440x1440", "3840x2160" };

        private void Awake()
        {
            SetupDropdown();

            dropdown.onValueChanged.AddListener(delegate { SetResolution(); });
        }

        private void SetupDropdown()
        {
            dropdown.ClearOptions();
            dropdown.AddOptions(resolutions);


            int res = -1;
            string currentResolution = Screen.currentResolution.width + "x" + Screen.currentResolution.height;

            for (int i = 0; i < resolutions.Count; i++)
            {
                if (resolutions[i] == currentResolution)
                    res = i;
            }

            //IF SCREEN'S CURRENT RESOLUTION DOES NOT MATCH ANY COMMON RESOLUTIONS, SET RESOLUTION TO DEFAULT VALUE
            if (res == -1)
                res = 13; //1920x1080

            dropdown.value = res;
        }

        private void SetResolution()
        {
            string resolution = resolutions[dropdown.value];
            string[] resolutionSplit = resolution.Split('x');

            int width = int.Parse(resolutionSplit[0]);
            int height = int.Parse(resolutionSplit[1]);

            Screen.SetResolution(width, height, FullScreenMode.Windowed);
        }

        private void OnDestroy()
        {
            dropdown.onValueChanged.RemoveListener(delegate { SetResolution(); });
        }
    }
}
