using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Ac1dwir3
{
    public class VolumeSlider : MonoBehaviour
    {
        [SerializeField]
        private Slider slider;

        private float multiplier = 30f;

        private void Start()
        {
            WorldAudioManager.Instance.audioMixer.SetFloat("Master Volume", Mathf.Log10(slider.value * multiplier));
        }
    }
}
