using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class ResolutionTests
{
    [Test]
    public void _String_Parses_Ints()
    {
        string _string = "1920x1080";

        string[] stringSplit = _string.Split('x');

        Assert.IsInstanceOf(typeof(int), int.Parse(stringSplit[0]));
    }

    [Test]
    public void _Width_Parses_From_String()
    {

        string _string = "1920x1080";

        string[] stringSplit = _string.Split('x');

        int width = int.Parse(stringSplit[0]);

        Assert.AreEqual(1920, width);
    }

    [Test]
    public void _Height_Parses_From_String()
    {

        string _string = "1920x1080";

        string[] stringSplit = _string.Split('x');

        int height = int.Parse(stringSplit[1]);

        Assert.AreEqual(1080, height);
    }
}
